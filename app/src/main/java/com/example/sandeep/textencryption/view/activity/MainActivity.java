package com.example.sandeep.textencryption.view.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.sandeep.textencryption.R;
import com.example.sandeep.textencryption.Utils.TextDataStorage;
import com.example.sandeep.textencryption.databinding.ActivityMainBinding;
import com.example.sandeep.textencryption.model.InputText;
import com.example.sandeep.textencryption.view.adapter.TextAdapter;
import com.example.sandeep.textencryption.viewmodel.MainViewModel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class MainActivity extends AppCompatActivity implements Observer{

    private ActivityMainBinding activityMainBinding;
    private MainViewModel mainViewModel;
    private DividerItemDecoration mDividerItemDecoration;

    public static Intent getIntent(Context context){
        Intent intent = new Intent(context,MainActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDataBinding();
        setSupportActionBar(activityMainBinding.toolbar);
        setUpListTxtView(activityMainBinding.listInputTxt);
        setupObserver(mainViewModel);
        mainViewModel.checkUserComeFirstTime();
    }

    private void setupObserver(Observable observable) {
        observable.addObserver(this);
    }

    private void setUpListTxtView(RecyclerView listInputTxt) {
        TextAdapter adapter = new TextAdapter();
        listInputTxt.setAdapter(adapter);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        listInputTxt.setLayoutManager(mLayoutManager);
        mDividerItemDecoration = new DividerItemDecoration(this, mLayoutManager.getOrientation());
        listInputTxt.addItemDecoration(mDividerItemDecoration);
    }

    private void initDataBinding() {
        activityMainBinding = DataBindingUtil.setContentView(this,R.layout.activity_main);
        mainViewModel = new MainViewModel(this);
        activityMainBinding.setMainViewModel(mainViewModel);
    }

    @Override
    public void update(Observable observable, Object o) {
        if(observable instanceof MainViewModel){
            TextAdapter textAdapter = (TextAdapter) activityMainBinding.listInputTxt.getAdapter();
            MainViewModel mainViewModel = (MainViewModel) observable;
            textAdapter.setInputTextList(mainViewModel.getInputTextList());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode, data);
        if(requestCode == 200 && resultCode == 200){
            String inputText = data.getStringExtra("MESSAGE");
            mainViewModel.changeInputDataSet(inputText);
        }
    }
}
