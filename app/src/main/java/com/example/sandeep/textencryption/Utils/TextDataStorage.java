package com.example.sandeep.textencryption.Utils;


import android.content.Context;
import android.util.Log;

import com.example.sandeep.textencryption.model.InputText;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class TextDataStorage {

    public static File createFile(Context context) {
        File path = context.getFilesDir();
        File file = new File(path, "encrypted_file.txt");
        return file;
    }

    public static void writeInFile(String data, Context context) {
        try {
            OutputStreamWriter outputStreamWriter =
                    new OutputStreamWriter(context.openFileOutput("encrypted_file.txt", Context.MODE_APPEND));
            outputStreamWriter.append(data);
            outputStreamWriter.append(System.lineSeparator());
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    public static List<InputText> readFromFile(Context context) {
        List<InputText> inputTextList = new ArrayList<>();
        try {
            InputStream inputStream = context.openFileInput("encrypted_file.txt");
            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receivingString = "";
                while ((receivingString = bufferedReader.readLine()) != null) {
                    try {
                        String decodedData = Encryption.decodeData(receivingString);
                        InputText inputText = new InputText(decodedData);
                        inputTextList.add(inputText);
                    }catch (Exception ex){

                    }
                }
                inputStream.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return inputTextList;
    }
}
