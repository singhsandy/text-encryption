package com.example.sandeep.textencryption;

import android.app.Application;

import com.example.sandeep.textencryption.Utils.Encryption;
import com.example.sandeep.textencryption.Utils.TextDataStorage;

import java.io.File;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;


public class EncryptionApplication extends Application {

    public static File file;
    public static SecretKeySpec secretKey = null;
    @Override
    public void onCreate(){
        super.onCreate();
        try {
            file = TextDataStorage.createFile(this);
        }catch (Exception ex){

        }
    }
}
