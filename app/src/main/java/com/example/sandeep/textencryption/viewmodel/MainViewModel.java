package com.example.sandeep.textencryption.viewmodel;


import android.app.Activity;
import android.content.Context;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.view.View;
import android.widget.Toast;
import com.example.sandeep.textencryption.R;
import com.example.sandeep.textencryption.Utils.Encryption;
import com.example.sandeep.textencryption.Utils.TextDataStorage;
import com.example.sandeep.textencryption.model.InputText;
import com.example.sandeep.textencryption.view.activity.InsertNewTextActivity;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class MainViewModel extends Observable {

    private Context context;
    private List<InputText> inputTextList;
    public ObservableInt inputProgress;
    public ObservableInt inputRecycler;
    public ObservableInt inputLabel;
    public ObservableField<String> messageLabel;

    public MainViewModel(Context context){
        this.context = context;
        this.inputTextList = new ArrayList<>();
        inputProgress = new ObservableInt(View.GONE);
        inputRecycler = new ObservableInt(View.GONE);
        inputLabel = new ObservableInt(View.VISIBLE);
        messageLabel = new ObservableField<>(context.getString(R.string.default_loading_people));
    }

    public void onClickFabLoad(View view) {
        initializeViews();
    }

    private void initializeViews() {
        ((Activity) context).startActivityForResult(InsertNewTextActivity.getIntent(context),200);
    }

    public List<InputText> getInputTextList() {
        try {
            String encodedString = Encryption.encodeData(inputTextList.get(0).getInputTxt());
            String decodeString = Encryption.decodeData(encodedString);
        }catch (Exception ex){

        }
        return inputTextList;
    }

    public void changeInputDataSet(String inputString) {
        inputTextList.add(new InputText(inputString));
        inputRecycler.set(View.VISIBLE);
        inputLabel.set(View.GONE);
        setChanged();
        notifyObservers();
    }


    // If file doesn't exist so user is coming first time otherwise there would be data in the file and fetch those data;
    public void checkUserComeFirstTime() {
            File file = new File(context.getFilesDir(),"encrypted_file.txt");
            if(file.exists()) {
                List<InputText> inputTexts = new ArrayList<>();
                for(InputText inputText:TextDataStorage.readFromFile(context) ){
                    if(inputText.getInputTxt() != null)
                        inputTexts.add(inputText);
                }
                if(inputTexts.size() >0){
                inputTextList.addAll(inputTexts);
                inputRecycler.set(View.VISIBLE);
                inputLabel.set(View.GONE);
                setChanged();
                notifyObservers();
                }else{
                    messageLabel.set(context.getString(R.string.wrong_pin_msg));
                }
            }
            else
                Toast.makeText(context, "NotExist", Toast.LENGTH_SHORT).show();
            
    }   
}
