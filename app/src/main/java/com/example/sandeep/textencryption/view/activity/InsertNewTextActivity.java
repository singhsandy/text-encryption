package com.example.sandeep.textencryption.view.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;

import com.example.sandeep.textencryption.R;
import com.example.sandeep.textencryption.databinding.ActivityInsertNewTextBinding;
import com.example.sandeep.textencryption.viewmodel.InsertNewTextViewModel;


public class InsertNewTextActivity extends AppCompatActivity {

    private ActivityInsertNewTextBinding activityInsertNewTextBinding;
    private InsertNewTextViewModel insertNewTextViewModel;

    public static Intent getIntent(Context context) {
        Intent intent = new Intent(context, InsertNewTextActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDataBinding();

    }

    private void initDataBinding() {
        activityInsertNewTextBinding = DataBindingUtil.setContentView(this, R.layout.activity_insert_new_text);
        insertNewTextViewModel = new InsertNewTextViewModel(this);
        activityInsertNewTextBinding.setInsertTextViewModel(insertNewTextViewModel);
        activityInsertNewTextBinding.newText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                insertNewTextViewModel.newTextInserted(editable.toString());
            }
        });
    }

}
