package com.example.sandeep.textencryption.Utils;

import android.util.Base64;
import android.util.Log;

import com.example.sandeep.textencryption.EncryptionApplication;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class Encryption {

    public static SecretKeySpec generateKey(String pin) throws NoSuchAlgorithmException, InvalidKeySpecException, UnsupportedEncodingException {

        char[] password = pin.toCharArray();
//        char[] testPassword = {'a', 'b', 'c', 'd'};
        byte[] baSalt = "'~7&amp;03~/.".getBytes();
        int keyLength = 128;
        int iterationCount = 10;
        SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        KeySpec keySpec = new PBEKeySpec(password,baSalt,iterationCount,keyLength);
        SecretKey secretKey = secretKeyFactory.generateSecret(keySpec);
        SecretKeySpec skeySpec = new SecretKeySpec(secretKey.getEncoded(), "AES");
        return skeySpec;
        /*byte[] binary = pin.getBytes("UTF-8");
        MessageDigest sha = MessageDigest.getInstance("SHA-1");
        binary = sha.digest(binary);
        binary = Arrays.copyOf(binary,16);
        return binary;*/
    }


    public static String encodeData(String data)
            throws NoSuchPaddingException,
            NoSuchAlgorithmException,
            InvalidKeySpecException,
            InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        byte[] encodeByte = null;
        String encodedString = null;
        try {
//            SecretKeySpec skeySpec = new SecretKeySpec(EncryptionApplication.secretKey,"AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, EncryptionApplication.secretKey , new IvParameterSpec(new byte[16]));
            byte[] base64TextToEncrypt = data.getBytes(Charset.forName("UTF-8"));
            encodeByte = cipher.doFinal(base64TextToEncrypt);
            encodedString = Base64.encodeToString(encodeByte, Base64.NO_WRAP);
        }catch (Exception ex){
            Log.e("Encoding Error", "encodeData: "+ ex.getMessage() );
        }
        return encodedString;
    }

    public static String decodeData(String encodedData)
            throws InvalidKeySpecException,
            NoSuchAlgorithmException,
            InvalidKeyException,
            NoSuchPaddingException {
        byte[] decodedBytes = null;
        String decodedString = null;
        try {
//            SecretKeySpec skeySpec = new SecretKeySpec(EncryptionApplication.secretKey,"AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, EncryptionApplication.secretKey, new IvParameterSpec(new byte[16]));

            byte[] base64TextToDecrypt = Base64.decode(encodedData,Base64.NO_WRAP);
            decodedBytes = cipher.doFinal(base64TextToDecrypt);
            decodedString = new String(decodedBytes, Charset.forName("UTF-8"));
        }catch (Exception exception){
            Log.e("Decode error", "decodeData: " + exception.getMessage() );
        }
        return decodedString;
    }

   /* private static IvParameterSpec generateIV() {
        IvParameterSpec IV;
        byte[] byteIV = new byte[16];
        generateSecureRandomKey().nextBytes(byteIV);
        IV = new IvParameterSpec(byteIV);
        return IV;
    }*/

   /* public static SecureRandom generateSecureRandomKey(){
        SecureRandom r = null;
        try {
            r = SecureRandom.getInstance("SHA1PRNG");
            byte[] newSeed = r.generateSeed(32);
            r.setSeed(newSeed);
        }catch (Exception ex){
            Log.e("Error in Secure Random", "generateSecureRandomKey: " + ex.getMessage() );
        }
        return r;
    }*/

}
