package com.example.sandeep.textencryption.viewmodel;


import android.app.Activity;
import android.content.Context;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.os.storage.StorageManager;
import android.util.Log;
import android.view.View;

import com.example.sandeep.textencryption.R;
import com.example.sandeep.textencryption.Utils.Encryption;
import com.example.sandeep.textencryption.Utils.TextDataStorage;
import com.example.sandeep.textencryption.model.InputText;
import com.example.sandeep.textencryption.view.activity.InsertNewTextActivity;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class MainViewModel extends Observable {

    private Context context;
    private List<InputText> inputTextList;
    public ObservableInt inputProgress;
    public ObservableInt inputRecycler;
    public ObservableInt inputLabel;
    public ObservableField<String> messageLabel;

    public MainViewModel(Context context){
        this.context = context;
        this.inputTextList = new ArrayList<>();
        inputProgress = new ObservableInt(View.GONE);
        inputRecycler = new ObservableInt(View.GONE);
        inputLabel = new ObservableInt(View.VISIBLE);
        messageLabel = new ObservableField<>(context.getString(R.string.default_loading_people));
    }

    public void onClickFabLoad(View view) {
        initializeViews();
    }

    private void initializeViews() {
        ((Activity) context).startActivityForResult(InsertNewTextActivity.getIntent(context),200);
    }

    public List<InputText> getInputTextList() {
        try {
            String encodedStr = Encryption.encodeData(inputTextList.get(0).getInputTxt());
            String decodeStr = Encryption.decodeData(encodedStr);
        }catch (Exception ex){
            Log.e("Sand", "getInputTextList: "+ ex.getMessage() );
        }
        return inputTextList;
    }

    public void changeInputDataSet(String inputString) {
        inputTextList.add(new InputText(inputString));
        inputRecycler.set(View.VISIBLE);
        inputLabel.set(View.GONE);
        setChanged();
        notifyObservers();
    }


    // if file is empty so user coming first time otherwise there would be data in the file and fetch those data;
    public void checkUserComeFirstTime() {
        try {
            BufferedReader br = new BufferedReader(new FileReader("path_to_some_file"));
            if (br.readLine() != null) {
                inputTextList.addAll(TextDataStorage.readFromFile(context));
                setChanged();
                notifyObservers();
            }
        }catch
                (Exception ex){}
    }
}
