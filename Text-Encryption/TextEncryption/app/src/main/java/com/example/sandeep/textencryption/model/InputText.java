package com.example.sandeep.textencryption.model;


import java.io.Serializable;

public class InputText implements Serializable {

    public InputText(String inputTxt) {
        this.inputTxt = inputTxt;
    }

    private String inputTxt;

    public String getInputTxt() {
        return inputTxt;
    }

    public void setInputTxt(String inputTxt) {
        this.inputTxt = inputTxt;
    }
}
