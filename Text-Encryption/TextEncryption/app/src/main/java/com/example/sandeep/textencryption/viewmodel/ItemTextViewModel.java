package com.example.sandeep.textencryption.viewmodel;


import android.content.Context;
import android.databinding.BaseObservable;

import com.example.sandeep.textencryption.model.InputText;

public class ItemTextViewModel extends BaseObservable {

    private InputText inputText;

    public ItemTextViewModel(InputText inputText){
        this.inputText = inputText;
    }

    public String getUserInput(){
        return inputText.getInputTxt();
    }

    public void setInputText(InputText inputText){
        this.inputText = inputText;
        notifyChange();
    }

}
