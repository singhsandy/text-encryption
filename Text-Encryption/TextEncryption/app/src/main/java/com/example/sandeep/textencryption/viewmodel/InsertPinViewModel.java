package com.example.sandeep.textencryption.viewmodel;


import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableField;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;

import com.example.sandeep.textencryption.EncryptionApplication;
import com.example.sandeep.textencryption.R;
import com.example.sandeep.textencryption.Utils.Encryption;
import com.example.sandeep.textencryption.view.activity.MainActivity;

public class InsertPinViewModel {
    public ObservableField<String> hintPinLabel;
    private Context context;
    private String pin;
    private static final String TAG = InsertPinViewModel.class.getName();

    public InsertPinViewModel(Context context){
        this.context = context;
        hintPinLabel = new ObservableField<>(context.getString(R.string.insert_pin));
    }

    public void onClickSubmitPinBtn(View view){
        if(pin!=null)
            context.startActivity(MainActivity.getIntent(context));
    }

    public void insertPin(String pin){
        this.pin = pin;
        try {
            EncryptionApplication.secretKey = Encryption.generateKey(pin);
        }catch (Exception ex){
            Log.e(TAG, "insertPin: " + ex.getMessage());
        }

    }
}
