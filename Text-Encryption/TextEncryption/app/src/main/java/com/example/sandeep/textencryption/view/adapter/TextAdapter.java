package com.example.sandeep.textencryption.view.adapter;


import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.example.sandeep.textencryption.R;
import com.example.sandeep.textencryption.databinding.ItemTextInputBinding;
import com.example.sandeep.textencryption.model.InputText;
import com.example.sandeep.textencryption.viewmodel.ItemTextViewModel;
import java.util.Collections;
import java.util.List;

public class TextAdapter extends RecyclerView.Adapter {

    private List<InputText> inputTextList;

    public TextAdapter(){
        this.inputTextList = Collections.emptyList();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemTextInputBinding itemTextInputBinding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_text_input,
                        parent,false);
        return new NewTextHolder(itemTextInputBinding);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((NewTextHolder)holder).bindItem(inputTextList.get(position));
    }

    @Override
    public int getItemCount() {
        return inputTextList.size();
    }

    public void setInputTextList(List<InputText> inputTextList) {
        this.inputTextList = inputTextList;
        notifyDataSetChanged();
    }

    public static class NewTextHolder extends RecyclerView.ViewHolder{

        ItemTextInputBinding mItemTextInputBinding;

        public NewTextHolder(ItemTextInputBinding itemTextInputBinding) {
            super(itemTextInputBinding.newInsertedText);
            this.mItemTextInputBinding = itemTextInputBinding;
        }

        public void bindItem(InputText inputText){
            if(mItemTextInputBinding.getItemViewModel() == null){
                mItemTextInputBinding.setItemViewModel(new ItemTextViewModel(inputText));
            }else{
                mItemTextInputBinding.getItemViewModel().setInputText(inputText);
            }
        }
    }
}
