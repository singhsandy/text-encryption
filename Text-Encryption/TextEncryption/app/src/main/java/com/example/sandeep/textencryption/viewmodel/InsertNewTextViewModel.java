package com.example.sandeep.textencryption.viewmodel;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableField;
import android.view.View;
import android.widget.Toast;

import com.example.sandeep.textencryption.R;
import com.example.sandeep.textencryption.Utils.Encryption;
import com.example.sandeep.textencryption.Utils.TextDataStorage;
import com.example.sandeep.textencryption.model.InputText;

public class InsertNewTextViewModel {

    private Context context;
    private String mInputText;
    public ObservableField<String> hintLabel;
    public ObservableField<String> submitLabel;
    public ObservableField<String> cancelLabel;
    public ObservableField<String> insertText;

    public InsertNewTextViewModel(Context context){
        this.context = context;
        hintLabel = new ObservableField<>(context.getString(R.string.insert_text));
        submitLabel = new ObservableField<>(context.getString(R.string.btn_add));
        cancelLabel = new ObservableField<>(context.getString(R.string.cancel_btn));
        insertText = new ObservableField<>("");
    }

    public void onClickAddBtn(View view) {
        Intent intent=new Intent();
        if(mInputText !=null && !mInputText.equals("")) {
            intent.putExtra("MESSAGE", mInputText);
            try {
                String encodedData = Encryption.encodeData(mInputText);
                TextDataStorage.writeInFile(encodedData, context);
                ((Activity) context).setResult(200, intent);
                ((Activity) context).finish();
            }catch(Exception ex){

            }
        }
    }

    public void onClickCancelBtn(View view) {
        ((Activity)context).finish();
    }

    public void newTextInserted(String s) {
        this.mInputText = s;
    }
}
