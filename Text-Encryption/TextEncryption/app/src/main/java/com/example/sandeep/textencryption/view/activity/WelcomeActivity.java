package com.example.sandeep.textencryption.view.activity;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;

import com.example.sandeep.textencryption.R;
import com.example.sandeep.textencryption.databinding.ActivityWelcomeBinding;
import com.example.sandeep.textencryption.viewmodel.InsertPinViewModel;

public class WelcomeActivity extends AppCompatActivity {

    private ActivityWelcomeBinding activityWelcomeBinding;
    private InsertPinViewModel insertPinViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        initDataBinding();
        setSupportActionBar(activityWelcomeBinding.toolbar);
    }

    private void initDataBinding() {
        activityWelcomeBinding = DataBindingUtil.setContentView(this, R.layout.activity_welcome);
        insertPinViewModel = new InsertPinViewModel(this);
        activityWelcomeBinding.setInsertPinViewModel(insertPinViewModel);
        activityWelcomeBinding.pin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                insertPinViewModel.insertPin(editable.toString());
            }
        });

    }
}
